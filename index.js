/* console.log("hello world"); */

//DOM -> allows us to access or modify the properties of an html element in a webpage.

//Selection of HTML Element -> document.querySelector
//Syntax -> document.querySelector("htmlElement"); -> ("htmlElement") or (".htmlElement")
//Alternative Syntax -> document.getElementbyId("htmlElement");

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const txtColor = document.querySelector("#text-color");


//Event Listeners -> user interaction is considered as an event.

//addEventListener -> take 2 arguments
//String -> string identifying an event
//Function -> will execute when a specified event is triggered
txtFirstName.addEventListener("keyup", (event) => {
    console.log(event.target);
    console.log(event.target.value);
});

txtFirstName.addEventListener("keyup", (event) => {
    spanFullName.innerHTML = `${txtFirstName.value}`;
});

const fullName = () => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value} `;
}

txtColor.addEventListener("change", (event) => {
    if (txtColor.value == "red") {
        spanFullName.style.color = "red";
    } else if (txtColor.value == "blue") {
        spanFullName.style.color = "blue";
    } else if (txtColor.value == "green") {
        spanFullName.style.color = "green";
    } else {
        spanFullName.style.color = "black";
    }

});

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);